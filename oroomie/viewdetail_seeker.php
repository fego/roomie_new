<?php
   include("db.php");
   $id=$_REQUEST['ID'];
   include_once("officeclass1.php");
   $OviewDetails=new cOffice();
   $OviewDetails->setID($id);
   $OviewDetails->ViewSeekerDetails();
   $query= $OviewDetails->getResultValue();
   $result=mysql_query($query);
   $row=@mysql_fetch_array($result);
   if(!mysql_num_rows($result))
   {
     printf("<script>location.href='viewfailuremessage.php'</script>");
   }
   $text="No";
   $right= '<img src=right.png></img>';
   $wrong= '<img src=wrong.png></img>';
?>
<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="pageTitle">Details of Provider</h2>
			</div>
		</div>
	</div>
</section>
<div class="container">
<p>&nbsp;</p>
<div class="tabbable"> <!-- Only required for left/right tabs -->
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab1" data-toggle="tab">Basic Information</a></li>
    <li><a href="#tab2" data-toggle="tab">Additional Information</a></li>
  </ul>
<div class="tab-content">
<div class="tab-pane active" id="tab1">
<p>&nbsp;</p>
<form role="form" name="form">
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Seeker ID</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['SeekerID']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Date of Posted</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Date_Created']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>No. of Seats</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['SeatsFrom']; ?> to <?php echo $row['SeatsTo']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Cost per seat</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['CostofSeatFrom']; ?> to <?php echo $row['CostofSeatTo']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Name</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Name']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Email</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Email']=='') { echo $text; } else { echo $row['Email']; }?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Mobile</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Mobile']=='') { echo $text; } else { echo $row['Mobile']; }?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>City</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['City']=='') { echo $text; } else { echo $row['City']; }?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Campus</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Campus']=='') { echo $text; } else { echo $row['Campus']; }?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Locality</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Locality']=='') { echo $text; } else { echo $row['Locality']; }?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Description</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Description']=='') { echo $text; } else { echo $row['Description']; }?></div>
        </div>
    </div>
	</form>
</div>
<div class="tab-pane" id="tab2">
<p>&nbsp;</p>
<form role="form" name="form1">
	<div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Air Conditioning &amp; Electricity</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['AC']=='') { echo $wrong; } if($row['AC']=='Y') { echo $right; } ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>WiFi / Broadband</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Broadband']=='') { echo $wrong; } if($row['Broadband']=='Y') { echo $right; } ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Copier / Printer</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Printer']=='') { echo $wrong; } if($row['Printer']=='Y') { echo $right; } ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>PC / Laptop</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Laptop']=='') { echo $wrong; } if($row['Laptop']=='Y') { echo $right; } ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Drinking Water</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Water']=='') { echo $wrong; } if($row['Water']=='Y') { echo $right; } ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Parking</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Parking']=='') { echo $wrong; } if($row['Parking']=='Y') { echo $right; } ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Reception</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Reception']=='') { echo $wrong; } if($row['Reception']=='Y') { echo $right; } ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Kitchen</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Kitchen']=='') { echo $wrong; } if($row['Kitchen']=='Y') { echo $right; } ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Cafeteria</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Cafeteria']=='') { echo $wrong; } if($row['Cafeteria']=='Y') { echo $right; } ?></div>
        </div>
    </div>
	<div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Meeting Room</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Meeting']=='') { echo $wrong; } if($row['Meeting']=='Y') { echo $right; } ?></div>
        </div>
    </div>
    <div class="form-group text-info"><b>Public Transport</b></div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Bus</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Bus']=='') { echo $wrong; } if($row['Bus']=='Y') { echo $right; } ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Metro</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Metro']=='') { echo $wrong; } if($row['Metro']=='Y') { echo $right; } ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Train</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['Train']=='') { echo $wrong; } if($row['Train']=='Y') { echo $right; } ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>ShareAuto</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php if($row['ShareAuto']=='') { echo $wrong; } if($row['ShareAuto']=='Y') { echo $right; } ?></div>
        </div>
    </div>
</form>
</div>
 </div><!--/.tab content-->
 	</div><!--/.tabable-->
		</div><!--/.container-->