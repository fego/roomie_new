<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Roomie</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://roomie.in" />
<!-- css -->
<link href="../css/bootstrap.min.css" rel="stylesheet" />
<link href="../css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="../css/jcarousel.css" rel="stylesheet" />
<link href="../css/flexslider.css" rel="stylesheet" />
<link href="../css/style.css" rel="stylesheet" />
 <link rel="shortcut icon" href="../img/favicon.png">
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
<div id="wrapper">

	<!-- start header -->
		<header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                  <a class="navbar-brand" href="index.html"><img src="../img/logo.png" alt="logo"/></a>
                </div>
<?php
 if((!isset($_REQUEST['active']))||($_REQUEST['active']=="SE"))
   {
   $class2="active";
   }
   if($_REQUEST['active']=="PO")
   {
   $class1="active";
   }
   if($_REQUEST['active']=="VB")
   {
   $class3="active";
   }
?>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
						<li class="<?php echo $class1; ?>"><a href="post.php?active=PO">Post</a></li>
						<li class="<?php echo $class2; ?>"><a href="search.php?active=SE">Search</a></li>
                        <li class="<?php echo $class3; ?>"><a href="viewbyid.php?active=VB">Viewby ID</a></li>
                        <li><a href="../index.html">Home</a></li> 
                    </ul>
                </div>
            </div>
        </div>
	</header><!-- end header -->