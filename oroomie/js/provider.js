// JavaScript Docume
var digits = "0123456789";
var phoneNumberDelimiters = "()-. ";
var validWorldPhoneChars = phoneNumberDelimiters + "+";
var minDigitsInIPhoneNumber = 10;
function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
      return true;
}
function trim(s)
{   var i;
    var returnString = "";
    for (i = 0; i < s.length; i++)
   {   
       var c = s.charAt(i);
        if (c != " ") returnString += c;
    }
    return returnString;
}
function stripCharsInBag(s, bag)
{  var i;
    var returnString = "";
      for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}
function checkInternationalPhone(strPhone){
var bracket=3
strPhone=trim(strPhone)
if(strPhone.indexOf("+")>1) return false
if(strPhone.indexOf("-")!=-1)bracket=bracket+1
if(strPhone.indexOf("(")!=-1 && strPhone.indexOf("(")>bracket)return false
var brchr=strPhone.indexOf("(")
if(strPhone.indexOf("(")!=-1 && strPhone.charAt(brchr+2)!=")")return false
if(strPhone.indexOf("(")==-1 && strPhone.indexOf(")")!=-1)return false
s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}
function validate()
{
    
	 var valid=document.form;		
     var digits = "0123456789";
	 var erColor="#E8E8FF";
   
     var iChars = "@#$%^+=[]\\\;{}|/<>!&*()?':=+_";
     
	 
      var Phone=document.form.mobile;
   
if(IsEmpty(valid.seats))
	{
		alert("Please enter your Total seats");
		valid.seats.style.background= erColor;
		valid.seats.focus();
		return false;
	}
	if(valid.seats.value!='')
	{
	 if(isNaN(valid.seats.value))
   {
     alert("Please enter numbers only");
	 valid.seats.style.background=erColor;
     valid.seats.focus();
     return (false);
   }
   }
	else
	{
	valid.seats.style.background= 'White';
	valid.seats.focus();
	}
   for (var i = 0; i < document.form.seats.value.length; i++) {
       if (iChars.indexOf(document.form.seats.value.charAt(i)) != -1)
       {
          alert ("Please enter text only");
	  valid.seats.focus();
          return false;
       }
    }
if(IsEmpty(valid.cost))
	{
		alert("Please enter your cost per seat");
		valid.cost.style.background= erColor;
		valid.cost.focus();
		return false;
	}
	if(valid.cost.value!='')
	{
	 if(isNaN(valid.cost.value))
   {
     alert("Please enter numbers only");
	 valid.cost.style.background=erColor;
     valid.cost.focus();
     return (false);
   }
   }
	else
	{
	valid.cost.style.background= 'White';
	valid.cost.focus();
	}
    if(IsEmpty(valid.name))
	{
		alert("Please Enter your Name");
		valid.name.style.background= erColor;
		valid.name.focus();
		return false;
	}
	else
	{
	valid.name.style.background= 'White';
	valid.name.focus();
	}
   for (var i = 0; i < document.form.name.value.length; i++) {
       if (iChars.indexOf(document.form.name.value.charAt(i)) != -1)
       {
          alert ("sorry, you entered special characters. \nThese are not allowed.\n");
	     valid.name.focus();
          return false;
       }
    }
     for (var i = 0; i < document.form.name.value.length; i++) 
     {
     if (digits.indexOf(document.form.name.value.charAt(i)) != -1) 
     {
        alert ("Sorry, you entered numbers. \nThese are not allowed.\n");
	    valid.name.focus();
        return false;
     }
    }
    if(IsEmpty(valid.mail))
	{
		alert("Please enter your mail-address ");
		valid.mail.style.background=erColor;
		valid.mail.focus();
		valid.mail.select();
		return false;
	}
	else
	{
	    valid.mail.style.background= 'White';
	    valid.mail.focus();
	 }
	if (valid.mail.value!="")
	{
		if (valid.mail.value.match(/[a-zA-Z0-9]+\@[a-zA-Z0-9-]+(\.(a-zA-Z0-9]{2}|[a-zA-Z0-9]{2}))+/)==null)
		{
			alert ("Please enter a valid mail-address");
	        valid.mail.style.background=erColor;
			valid.mail.focus();
			return false;
		}
		else
		{
			valid.mail.style.background= 'White';
	         valid.mail.focus();
	    }
	}	
   
	 if ((Phone.value==null)||(Phone.value=="")){
		alert("Please enter your contact number")
        valid.mobile.style.background=erColor;
		Phone.focus()
		return false;
	}
	if (checkInternationalPhone(Phone.value)==false){
		alert("Please enter a valid contact number")
                valid.mobile.style.background=erColor;
		Phone.focus()
		return false;
	}
    if(valid.campus.value=="")
	{
		alert("Please select your campus ");
		valid.campus.style.background=erColor;
		valid.campus.focus();
		return false;
	}
	else
	{
	valid.campus.style.background= 'White';
	valid.campus.focus();
	}
   
	if(valid.city.value=="")
	{
		alert("Please select your city ");
		valid.city.style.background=erColor;
		valid.city.focus();
		return false;
	}
	else
	{
	valid.city.style.background= 'White';
	valid.city.focus();
	}
	if(valid.locality.value=="")
	{
		alert("Please enter your locality");
		valid.locality.style.background=erColor;
		valid.locality.focus();
		return false;
	}
     for (var i = 0; i < document.form.locality.value.length; i++) {
		  var validChars = "@#$%^+=[]\\\';{}.,-()?/*&!|<>0123456789";
       if (validChars.indexOf(document.form.locality.value.charAt(i)) != -1)
       {
          alert ("Please enter text only");
	      valid.locality.focus();
          return false;
       }
    }
	 if(valid.address.value!='')
	{
      for (var i = 0; i < document.form.address.value.length; i++) {
       if (iChars.indexOf(document.form.address.value.charAt(i)) != -1)
       {
          alert("Please enter numbers and characters only");
           valid.address.style.background=erColor;
	      valid.address.focus();
          return false;
       }
       }
		
	}
	else
	{
	valid.address.style.background= 'White';
	valid.address.focus();
	}
        
    
	 /*for (var i = 0; i < document.form.locality.value.length; i++) 
     {
     if (digits.indexOf(document.form.locality.value.charAt(i)) != -1) 
     {
        alert ("Sorry, you entered numbers. \nThese are not allowed.\n");
	    valid.locality.focus();
        return false;
     }
    }
	else
	{
	valid.locality.style.background= 'White';
	valid.locality.focus();
	}
   
	/*if(IsEmpty(valid.rent))
	{
		alert("Please enter your rent of room");
		valid.rent.style.background=erColor;
		valid.rent.focus();
		return false;
	}
	else
	{
	valid.rent.style.background= 'White';
	valid.rent.focus();
	}
	  if(isNaN(valid.rent.value))
   {
     alert("Please enter numbers only");
	 valid.rent.style.background=erColor;
     valid.rent.focus();
     return (false);
   }
   else
	{
	valid.rent.style.background= 'White';
	valid.rent.focus();
	}
	
	if(valid.advance.value!='')
	{
         for (var i = 0; i < document.form.advance.value.length; i++) 
         {
          var aboutchars="@#$^&()*~?/{}[]";
          if (aboutchars.indexOf(document.form.advance.value.charAt(i)) != -1)
          {
          alert ("Please enter  valid characters only");
          valid.advance.style.background=erColor;
	      valid.advance.focus();
          return false;
       }
       }
    }*/
	/*if(isNaN(gf.advance.value))
   {
     alert("Please enter numbers only");
	 gf.advance.style.background=erColor;
     gf.advance.focus();
     return (false);
   }
   else
	{
	gf.advance.style.background= 'White';
	gf.advance.focus();
	}*/
	
	 if(IsEmpty(valid.description))
	{
		alert("Please enter your office space description");
		valid.description.style.background=erColor;
		valid.description.focus();
		return false;
	}
	else
	{
	valid.description.style.background= 'White';
	valid.description.focus();
	}
	 for (var i = 0; i < document.form.description.value.length; i++) 
     {
          var aboutchars="@#$^&()*~?/{}[]";
       if (aboutchars.indexOf(document.form.description.value.charAt(i)) != -1)
       {
          alert ("Please enter  valid characters only");
          valid.description.style.background=erColor;
	      valid.description.focus();
          return false;
       }
    }
					for (var i = 0; i < document.form.nearest.value.length; i++)
				{   
				if (iChars.indexOf(document.form.nearest.value.charAt(i)) != -1)
				{   
					alert ("The nearest field has special characters. \nThese are not allowed.\n");	
					valid.nearest.style.background=erColor;	
					valid.nearest.focus(); 
					return false;  
				}   
				}	
				for (var i = 0; i < document.form.nearest.value.length; i++) 
				{   
				if (digits.indexOf(document.form.nearest.value.charAt(i)) != -1) 
				{   
					alert ("The nearest field has special characters. \nThese are not allowed.\n");
					valid.nearest.style.background=erColor;	
					valid.nearest.focus();  
					return false;    
				}   
				}
   
   if(isNaN(valid.distance.value))
   {
     alert("Please enter numbers only");
	 valid.distance.style.background=erColor;
     valid.distance.focus();
     return (false);
   }
	else
	{
	valid.distance.style.background= 'White';
	valid.distance.focus();
    }
  function IsEmpty(obj)
  {
	var objValue;
	objValue = obj.value.replace(/\s+$/,"");
	if(objValue.length == 0)
	{
	  return true;
	} 
	else
	{
	   return false;
	}
  }
}