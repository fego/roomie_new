<script type="text/javascript">
function validate()
{
	 var valid=document.form1;		
     var digits = "0123456789";
	 var erColor="#E8E8FF";
   
     var iChars = "@#$%^()+=-[]\\\';/{}|:<>";
	 var x=document.getElementById('provider');
	 var y=document.getElementById('seeker');
    
    if(x.checked==false&&y.checked==false)
    {
      alert("please select search for Provider / Seeker");
      document.getElementById('provider').focus();
      
      return false; 
    }
	
   if(valid.City.value=="")
	{
		alert("Please select you are city");
		valid.City.style.background= erColor;
		valid.City.focus();
		return false;
	}
	else
	{
	   valid.City.style.background= 'white';
		valid.City.focus();
   }
  
	
}
</script>
<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="pageTitle">Search</h2>
			</div>
		</div>
	</div>
</section>
<div class="container">
<p>&nbsp;</p>
<form role="form" action="providersearchresult.php?active=SE" method="post"  name="form1" id="form1" onSubmit="return validate();">
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-3"><span class="text-danger">*</span> <b>Search for</b> :</div>
        <div class="col-md-3"><input name="searchfor" type="radio" value="P" id="provider" <?php if($_REQUEST['search']=="provider"){ echo "checked";}?> /> Provider</div>
        <div class="col-md-3"><input name="searchfor" type="radio" id="seeker" value="S" <?php if($_REQUEST['search']=="seeker"){ echo "checked";}?> /> Seeker</div>
        </div>
    </div>
  <div class="form-group">
    	<div class="row">
    	<div class="col-md-3"><b>Campus</b> :</div>
        <div class="col-md-3"><select name="campus" id="campus" class="form-control">
            <option selected="selected" value="All">All</option>
            <?php
			require_once("common_inc.php");
			foreach ($arrCampus as $value)
				{
			echo '<option value="';echo $value;echo '";>';
			echo "$value";
			echo '</option>';echo "\n";
				}
		?>
        </select></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-3"><b>City</b> :</div>
        <div class="col-md-3"><select name="City" id="City" class="form-control">
            <option selected="selected" value="All">All</option>
            <?php
			require_once("common_inc.php");
			foreach ($arrCity as $value)
				{
			echo '<option value="';echo $value;echo '";>';
			echo "$value";
			echo '</option>';echo "\n";
				}
		?>
        </select></div>
        <div class="col-md-3"> <b>Locality</b> :</div>
        <div class="col-md-3"><input type="text" name="Locality" maxlength="50" class="form-control"></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-3"> <b>No.of seats</b> :</div>
        <div class="col-md-3"><input type="text" name="SeatsFrom" maxlength="50" class="form-control"/></div>
        <div class="col-md-3"> <b>To</b> :</div>
        <div class="col-md-3"><input type="text" name="SeatsTo" maxlength="50" class="form-control"></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-3"> <b>Cost of Seat</b> :</div>
        <div class="col-md-3"><input type="text" name="CostFrom" maxlength="50" class="form-control"/></div>
        <div class="col-md-3"> <b>To</b> :</div>
        <div class="col-md-3"><input type="text" name="CostTo" maxlength="50" class="form-control"></div>
        </div>
    </div>
	<div class="form-group text-info"><b>Posted in Last</b></div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-3"><input name="date" type="radio" value="A" /> 1 Week</div>
        <div class="col-md-3"><input name="date" type="radio" value="B" /> 2 Week</div>
        <div class="col-md-3"><input name="date" type="radio" value="C" /> 1 Month</div>
        <div class="col-md-3"><input name="date" type="radio" value="D" /> All</div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-3"> <b>Sort by</b> :</div>
        <div class="col-md-3"><select name="sortby" class="form-control">
            <!--<option>City</option>
            <option>Seats</option>-->
             <option value="CostofSeat">Cost</option>
            <option value="Date_Created">Date</option>
          </select></div>
        </div>
    </div>
    <div class="form-group" align="right">
        <button type="submit" name="Submit" class="btn btn-primary" value="Search">Search</button>
    </div>
</form>
</div><!--/.container-->