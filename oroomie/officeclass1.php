<?php
	class cOffice
	{
	   private $m_nID;
	   private $m_nSeats;
	   private $m_nSeatsTo;
	   private $m_nCostOfSeat;
	   private $m_nCostofSeatTo;
	   private $m_strName;
	   private $m_strEmail;
	   private $m_nMobile;
	   private $m_strCity;
	   private $m_strLocality;
	   private $m_strAddress;
	   private $m_strDescription;
	   private $m_nRent;
	   private $m_nRentTo;
	   private $m_nAdvance;
	   private $m_strAC;
	   private $m_strBroadband;
	   private $m_strWater;
	   private $m_strPrinter;
	   private $m_strLaptop;
	   private $m_strParking;
	   private $m_strReception;
	   private $m_strKitchen;
	   private $m_strCafeteria;
	   private $m_strMeeting;
	   private $m_strPublicTransport;
	   private $m_strNearestStop;
	   private $m_nDistance;
	   private $m_nDate;
	   private $m_strSeatField;
	   private $m_strSeatToField;
	   private $m_strCostofSeatField;
	   private $m_strCostofSeatToField;
	   private $m_strRentField;
	   private $m_strRentToField;
	   private $m_strCityField;
	   private $m_strLocalityField;
	   private $m_strDBResult;
	   private $m_nPage;
	   private $_nAcode;
	   private $m_strBus;
	   private $m_strMetro;
	   private $m_strTrain;
	   private $m_strAuto;
	   private $m_strURL;
       private $m_strSortby;
	   private $m_strCampus;
	   private $m_strCampusField;
	    function setURL($URL)
	   {
	     $this->m_strURL=$URL;
	   }
	   function setCampus($campus)
	   {
	     $this->m_strCampus=$campus;
	   }
	   function setCampusField($campusField)
	   {
	     $this->m_strCampusField=$campusField;
	   }
	   function setBus($bus)
	   {
	     $this->m_strBus=$bus;
	   }
	   function setMetro($metro)
	   {
	     $this->m_strMetro=$metro;
	   }
	   function setTrain($train)
	   {
	     $this->m_strTrain=$train;
	   }
	   function setAuto($auto)
	   {
	     $this->m_strAuto=$auto;
	   }
	   function setActivationCode($code)
	   {
	     $this->m_nAcode=$code;
	   }
	   function setSeatField($seatfield)
	   {
	      $this->m_strSeatField=$seatfield;
	   }
	   function setSeatToField($seattofield)
	   {
	      $this->m_strSeatToField=$seattofield;
	   }
	   function setCostofSeatField($costfield)
	   {
	      $this->m_strCostofSeatField=$costfield;
	   }
	    function setCostofSeatToField($costfield)
	   {
	      $this->m_strCostofSeatToField=$costfield;
	   }
	   function setRentField($rentfield)
	   {
	     $this->m_strRentField=$rentfield;
	   }
	   function setRentToField($rentfield)
	   {
	     $this->m_strRentToField=$rentfield;
	   }
	   function setCityField($cityfield)
	   {
	     $this->m_strCityField=$cityfield;
	   }
	   function setLocalityField($locality)
	   {
	      $this->m_strLocalityField=$locality;
	   }
	   function setResultValue($result)
	   {
	      $this->m_strDBResult=$result;
	   }
	   function getResultValue()
	   {
	     return $this->m_strDBResult;
	   }
	   function setID($ID)
	   {
	     $this->m_nID=$ID;
	   }
	   function setNumberofSeats($seats)
	   {
	      $this->m_nSeats=$seats;
	   }
	   function getNumberofSeats()
	   {
	      return $this->m_nSeats;
	   }
	   function setNumberofSeatsTo($seats)
	   {
	      $this->m_nSeatsTo=$seats;
	   }
	   function getNumberofSeatsTo()
	   {
	      return $this->m_nSeatsTo;
	   }
	   function setCostofSeat($cost)
	   {
	      $this->m_nCostOfSeat= $cost;
	   }
	  
	   function getCostofSeat()
	   {
	     return $this->m_nCostOfSeat;
	   }
	    function setCostofSeatTo($costto)
	   {
	      $this->m_nCostOfSeatTo= $costto;
	   }
	   function getCostofSeatTo()
	   {
	      return $this->m_nCostOfSeatTo;
	   }
	   function setName($name)
	   {
	     $this->m_strName=$name;
	   }
	   function setEmail($email)
	   {
	     $this->m_strEmail=$email;
	   }
	   function setMobileNumber($mobile)
	   {
	     $this->m_nMobile=$mobile;
	   }
	   function setCity($city)
	   {
	      $this->m_strCity=$city;
	   }
	   function getCity()
	   {
	      return $this->m_strCity;
	   }
	   function setLocality($locality)
	   {
	      $this->m_strLocality=$locality;
	   }
	   function getLocality()
	   {
	      return $this->m_strLocality;
	   }
	   function setAddress($address)
	   {
	     $this->m_strAddress=$address;
	   }
	   function setDescription($description)
	   {
	     $this->m_strDescription=$description;
	   }
	   function setRent($rent)
	   {
	     $this->m_nRent=$rent;
	   }
	   function getRent()
	   {
	     return $this->m_nRent;
	   }
	    function setRentTo($rentto)
	   {
	     $this->m_nRentTo=$rentto;
	   }
	   function getRentTo()
	   {
	     return $this->m_nRentTo;
	   }
	   function setAdvance($advance)
	   {
	     $this->m_nAdvance=$advance;
	   }
	   function setAC($AC)
	   {
	     $this->m_strAC=$AC;
	   }
	   function setBroadBand($broadband)
	   {
	     $this->m_strBroadband=$broadband;
	   }
	   function setWater($water)
	   {
	     $this->m_strWater=$water;
	   }
	   function setPrinter($printer)
	   {
	     $this->m_strPrinter=$printer;
	   }
	   function setLaptop($pc)
	   {
	     $this->m_strLaptop=$pc;
	   }
	   function setParking($parking)
	   {
	     $this->m_strParking=$parking;
	   }
	   function setReception($reception)
	   {
	      $this->m_strReception=$reception;
	   }
	   function setKitchen($kitchen)
	   {
	      $this->m_strKitchen=$kitchen;
	   }
       function setCafeteria($cafeteria)
	   {
	      $this->m_strCafeteria=$cafeteria;
	   }
	    function setMeeting($meeting)
	   {
	      $this->m_strMeeting=$meeting;
	   }
	   function setPublicTransport($transport)
	   {
	     $this->m_strPublicTransport=$transport;
	   }
	   function setNearestStop($nearest)
	   {
	     $this->m_strNearestStop=$nearest;
	   }
	   function setDistance($distance)
	   {
	     $this->m_strDistance=$distance;
	   }
	   function setDate($date)
	   {
	      $this->m_nDate=$date;
	   }
	   function setPagination($page)
	   {
	     $this->m_nPage=$page;
	   }
	   function getPagination()
	   {
	     return $this->m_nPage;
	   }
           function setSortby($sortby)
	   {
	      $this->m_strSortby=$sortby;
	   }
	   /*
		 Name:PostSeatProvider()
		 Input: None
		 Output: store the user data to provider table 
		*/
	   function PostSeatProvider()
	   {
	     $query="INSERT INTO provider(ProviderID,Seats,CostofSeat,Name,Email,Mobile,City,Locality,Address,Description,Rent,Advance,AC,Broadband,Water,Printer,Laptop,Parking,Reception,Kitchen,Cafeteria,NearestStop,Distance,Date_Created,Activation_Code,Bus,Metro,Train,ShareAuto,URL,Meeting,Campus) VALUES('".$this->m_nID."','".$this->m_nSeats."','".$this->m_nCostOfSeat."','".$this->m_strName."','".$this->m_strEmail."','".$this->m_nMobile."','".$this->m_strCity."','".$this->m_strLocality."','".$this->m_strAddress."','".$this->m_strDescription."','".$this->m_nRent."','".$this->m_nAdvance."','".$this->m_strAC."','".$this->m_strBroadband."','".$this->m_strWater."','".$this->m_strPrinter."','".$this->m_strLaptop."','".$this->m_strParking."','".$this->m_strReception."','".$this->m_strKitchen."','".$this->m_strCafeteria."','".$this->m_strNearestStop."','".$this->m_strDistance."','".$this->m_nDate."','".$this->m_nAcode."','".$this->m_strBus."','".$this->m_strMetro."','".$this->m_strTrain."','".$this->m_strAuto."','".$this->m_strURL."','".$this->m_strMeeting."','".$this->m_strCampus."')";
		 $res=mysql_query($query);
	   }//function ends
	    /*
		 Name:PostSeatSeeker()
		 Input: None
		 Output: store the user data to seeker table 
		*/
	   function PostSeatSeeker()
	   {
	   $query="INSERT INTO seeker(SeekerID,SeatsFrom,SeatsTo,CostofSeatTo,CostofSeatFrom,Name,Email,Mobile,City,Locality,Description,RentFrom,RentTo,Advance,AC,Broadband,Water,Printer,Laptop,Parking,Reception,Kitchen,Cafeteria,Date_Created,Bus,Metro,Train,ShareAuto,Meeting,Activation_Code,Campus) VALUES('".$this->m_nID."','".$this->m_nSeats."','".$this->m_nSeatsTo."','".$this->m_nCostOfSeatTo."','".$this->m_nCostOfSeat."','".$this->m_strName."','".$this->m_strEmail."','".$this->m_nMobile."','".$this->m_strCity."','".$this->m_strLocality."','".$this->m_strDescription."','".$this->m_nRent."','".$this->m_nRentTo."','".$this->m_nAdvance."','".$this->m_strAC."','".$this->m_strBroadband."','".$this->m_strWater."','".$this->m_strPrinter."','".$this->m_strLaptop."','".$this->m_strParking."','".$this->m_strReception."','".$this->m_strKitchen."','".$this->m_strCafeteria."','".$this->m_nDate."','".$this->m_strBus."','".$this->m_strMetro."','".$this->m_strTrain."','".$this->m_strAuto."','".$this->m_strMeeting."','".$this->m_nAcode."','".$this->m_strCampus."')";
		 $res=mysql_query($query);
	   }//function ends
	    /*
		 Name:SeatProviderSearchResult()
		 Input: None
		 Output: retrive the user match criteria and give the result of seats provider
		*/
		function SeatProviderSearchResult()
		{
		   
			  $tbl_name="provider";	
			  $curDate=date("Y-m-d");
			  $date=$_REQUEST['date'];
			  $sortby=$_REQUEST['sortby'];
			  if($sortby=='Date_Created')
			  {
			    $dsc="desc";
			  }
			  else
			  {
			    $dsc="asc";
			  }
			  	//your table name
	     // How many adjacent pages should be shown on each side?
	     $adjacents = 3;
	
		/* 
		   First get total number of rows in data table. 
		   If you have a WHERE clause in your query, make sure you mirror it here.
		*/
		$query = "SELECT COUNT(*) as num FROM $tbl_name ";
			if($_REQUEST['City']!='All')
			{
				$query.= " WHERE ".$this->m_strCityField." = '".$this->m_strCity."'";
			}
			if($_REQUEST['campus']!='All')
			{
				$query.= " WHERE ".$this->m_strCampusField." = '".$this->m_strCampus."'";
			}
			if($_REQUEST['Locality']!='')
			{
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!=''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']==''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!=''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']==''))
			{
				  $query.=" WHERE ";
			}
	        	 $query.=" ".$this->m_strLocalityField." = '".$this->m_strLocality."'";
			}
			if(($_REQUEST['SeatsFrom']!='')&&($_REQUEST['SeatsTo']!=''))
			{
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']==''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']==''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']==''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']==''))
			{
		 		 $query.=" WHERE ";
			}
				$query.= " ".$this->m_strSeatField." BETWEEN '".$this->m_nSeats."' AND '".$this->m_nSeatsTo."'";
			}
		 	if(($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!=''))
		 	{
				if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!=''))
				{
					 $query.=" AND ";
				}
				if(($_REQUEST['City']!='All')&&($_REQUEST['campus']==''))
				{
					 $query.=" AND ";
				}
				if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!=''))
				{
					 $query.=" AND ";
				}
				if(($_REQUEST['City']=='All')&&($_REQUEST['campus']==''))
				{
					  $query.=" WHERE ";
				}
			
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!='')&&($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')&&($_REQUEST['Locality']=='')))
			{
		 		 $query.=" WHERE ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!='')&&($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')&&($_REQUEST['Locality']!='')))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['SeatsFrom']!='')&&($_REQUEST['SeatsTo']!='')))
			{
		 		 $query.=" AND ";
			}
		 		  $query.=" ".$this->m_strCostofSeatField." BETWEEN '".$this->m_nCostOfSeat."' AND '".$this->m_nCostOfSeatTo."'";
			} 
	 	    
			if($_REQUEST['date']!='')
	        {
	       if(($_REQUEST['date']!='D')&&($_REQUEST['City']!='All')&&($_REQUEST['campus']!='All'))
	       {
              $query.=" AND (";
            }
		  /* if(($_REQUEST['date']!='D')&&($_REQUEST['City']=='All')&&(($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')||($_REQUEST['CostFrom']=='')&&($_REQUEST['CostTo']=='')))
	       {
              $query.=" AND (";
            }*/
			if(($_REQUEST['date']!='D')&&($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')||($_REQUEST['CostFrom']=='')&&($_REQUEST['CostTo']=='')||($_REQUEST['Locality']=='')))
			{
			   $query.=" WHERE (";
			}
            if(($_REQUEST['date']!='D')&&($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['SeatsFrom']!='')&&($_REQUEST['SeatsTo']!='')||($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!='')||($_REQUEST['Locality']!='')))
			{
			   $query.=" AND (";
			}
           if($_REQUEST['date']=='A')
 	       {
		      $oneweek = mktime(0,0,0,date("m"),date("d")-7,date("Y"));
			  $oneweek_val = date("Y-m-d", $oneweek);
	          $query .= " provider.Date_Created BETWEEN '".$oneweek_val."' AND '".$curDate."' ";
	       }
	       if($_REQUEST['date']=='B')
	       {
		       $twoweek = mktime(0,0,0,date("m"),date("d")-14,date("Y"));
			   $twoweek_val = date("Y-m-d", $twoweek);
  	           $query .= " provider.Date_Created BETWEEN '".$twoweek_val."' AND '".$curDate."' ";
	        }
	       if($_REQUEST['date']=='C')
	       {
		      $onemonth = mktime(0,0,0,date("m"),date("d")-30,date("Y"));
			  $month_val = date("Y-m-d", $onemonth);
	          $query .= " provider.Date_Created BETWEEN '".$month_val."' AND '".$curDate."' ";
	       }
	     }
         if(($_REQUEST['date']=='A') || ($_REQUEST['date']=='B') || ($_REQUEST['date']=='C'))
	     {
             $query .=")";
	     }
		 //$query.=" AND IsActive='Y'";	
			//echo $_REQUEST['city'];
	     /*if(($_REQUEST['RentFrom']!='')&&($_REQUEST['RentTo']!=''))
	     {
	        $query.=" AND ".$this->m_strRentField." BETWEEN ".$this->m_nRent." AND ".$this->m_nRentTo."";
	     }	*/
		$total_pages = mysql_fetch_array(mysql_query($query));
		$total_pages = $total_pages['num'];
		
		/* Setup vars for query. */
		$targetpage = "providersearchresult.php"; 	//your file name  (the name of this file)
		$limit = 5; 								//how many items to show per page
		$page =@mysql_escape_string($_GET['page']);
		if($page) 
			$start = ($page - 1) * $limit; 			//first item to display on this page
		else
			$start = 0;						
 
		  $query1="SELECT * FROM $tbl_name";
			if($_REQUEST['City']!='All')
			{
				$query1.= " WHERE ".$this->m_strCityField." = '".$this->m_strCity."'";
			}
			if($_REQUEST['campus']!='All')
			{
				$query1.= " WHERE ".$this->m_strCampusField." = '".$this->m_strCampus."'";
			}
			if($_REQUEST['Locality']!='')
			{
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!=''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']==''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!=''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']==''))
			{
				  $query1.=" WHERE ";
			}
	        	 $query1.=" ".$this->m_strLocalityField." = '".$this->m_strLocality."'";
			}
			if(($_REQUEST['SeatsFrom']!='')&&($_REQUEST['SeatsTo']!=''))
			{
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']==''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']==''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']==''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']==''))
			{
		 		 $query1.=" WHERE ";
			}
				$query1.= " ".$this->m_strSeatField." BETWEEN '".$this->m_nSeats."' AND '".$this->m_nSeatsTo."'";
			}
		 	if(($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!=''))
		 	{
				if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!=''))
				{
					 $query1.=" AND ";
				}
				if(($_REQUEST['City']!='All')&&($_REQUEST['campus']==''))
				{
					 $query1.=" AND ";
				}
				if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!=''))
				{
					 $query1.=" AND ";
				}
				if(($_REQUEST['City']=='All')&&($_REQUEST['campus']==''))
				{
					  $query1.=" WHERE ";
				}
			
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!='')&&($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')&&($_REQUEST['Locality']=='')))
			{
		 		 $query1.=" WHERE ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!='')&&($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')&&($_REQUEST['Locality']!='')))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['SeatsFrom']!='')&&($_REQUEST['SeatsTo']!='')))
			{
		 		 $query1.=" AND ";
			}
		 		  $query1.=" ".$this->m_strCostofSeatField." BETWEEN '".$this->m_nCostOfSeat."' AND '".$this->m_nCostOfSeatTo."'";
			} 
	 	    
			if($_REQUEST['date']!='')
	        {
	       if(($_REQUEST['date']!='D')&&($_REQUEST['City']!='All')&&($_REQUEST['campus']!='All'))
	       {
              $query1.=" AND (";
            }
		  /* if(($_REQUEST['date']!='D')&&($_REQUEST['City']=='All')&&(($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')||($_REQUEST['CostFrom']=='')&&($_REQUEST['CostTo']=='')))
	       {
              $query.=" AND (";
            }*/
			if(($_REQUEST['date']!='D')&&($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')||($_REQUEST['CostFrom']=='')&&($_REQUEST['CostTo']=='')||($_REQUEST['Locality']=='')))
			{
			   $query1.=" WHERE (";
			}
            if(($_REQUEST['date']!='D')&&($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['SeatsFrom']!='')&&($_REQUEST['SeatsTo']!='')||($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!='')||($_REQUEST['Locality']!='')))
			{
			   $query1.=" AND (";
			}
           if($_REQUEST['date']=='A')
 	       {
		      $oneweek = mktime(0,0,0,date("m"),date("d")-7,date("Y"));
			  $oneweek_val = date("Y-m-d", $oneweek);
	          $query1 .= " provider.Date_Created BETWEEN '".$oneweek_val."' AND '".$curDate."' ";
	       }
	       if($_REQUEST['date']=='B')
	       {
		       $twoweek = mktime(0,0,0,date("m"),date("d")-14,date("Y"));
			   $twoweek_val = date("Y-m-d", $twoweek);
  	           $query1.= " provider.Date_Created BETWEEN '".$twoweek_val."' AND '".$curDate."' ";
	        }
	       if($_REQUEST['date']=='C')
	       {
		      $onemonth = mktime(0,0,0,date("m"),date("d")-30,date("Y"));
			  $month_val = date("Y-m-d", $onemonth);
	          $query1 .= " provider.Date_Created BETWEEN '".$month_val."' AND '".$curDate."' ";
	       }
	     }
         if(($_REQUEST['date']=='A') || ($_REQUEST['date']=='B') || ($_REQUEST['date']=='C'))
	     {
             $query1 .=")";
	     }	
	     /*if(($_REQUEST['RentFrom']!='')&&($_REQUEST['RentTo']!=''))
	     {
	        $query1.=" AND ".$this->m_strRentField." BETWEEN ".$this->m_nRent." AND ".$this->m_nRentTo."";
	     }	*/
				
		  $query1.=" order by $sortby $dsc LIMIT $start, $limit";
		  //echo $query1;
		  $this->setResultValue($query1);
		  //$result=mysql_query($query1);
		  
		  /* Setup page vars for display. */
		if ($page == 0) $page = 1;					//if no page var is given, default to 1.
		$prev = $page - 1;							//previous page is page - 1
		$next = $page + 1;							//next page is page + 1
		$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
		$lpm1 = $lastpage - 1;						//last page minus 1
		
		/* 
			Now we apply our rules and draw the pagination object. 
			We're actually saving the code to a variable in case we want to draw it more than once.
		*/
		$pagination = "";
		if($lastpage > 1)
		{	
			$pagination .= "<ul class=\"pagination pagination-sm\">";
			//previous button
			if ($page > 1) 
				$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$prev\">previous</a></li>";
			else
				$pagination.= "<li class=\"disabled\"><span>previous</span></li>";	
			
			//pages	
			if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
			{	
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li class=\"active\"><span>$counter</span></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$counter\">$counter</a></li>";					
				}
			}
			elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if($page < 1 + ($adjacents * 2))		
				{
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active\"><span>$counter</span></li>";
						else
							$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$counter\">$counter</a></li>";					
					}
					$pagination.= "<li><span>...</span></li>";
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$lpm1\">$lpm1</a></li>";
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$lastpage\">$lastpage</a></li>";		
				}
				//in middle; hide some front and some back
				elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
				{
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=1\">1</a></li>";
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=2\">2</a></li>";
					$pagination.= "<li><span>...</span></li>";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active\"><span>$counter</span></li>";
						else
							$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$counter\">$counter</a></li>";					
					}
					$pagination.= "<li><span>...</span></li>";
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$lpm1\">$lpm1</a></li>";
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$lastpage\">$lastpage</a></li>";		
				}
				//close to end; only hide early pages
				else
				{
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=1\">1</a></li>";
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=2\">2</a></li>";
					$pagination.= "<li><span>...</span></li>";
					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active\"><span>$counter</span></li>";
						else
							$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$counter\">$counter</a></li>";					
					}
				}
			}
			
			//next button
			if ($page < $counter - 1) 
				$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$next\">next</a></li>";
			else
				$pagination.= "<li class=\"disabled\"><span>next</span></li>";
			$pagination.= "</ul>\n";	
			$this->setPagination($pagination);	
		}
		
		
		}//function ends
		 /*
		 Name:SeatSeekerSearchResult()
		 Input: None
		 Output: retrive the user match criteria and give the result of seats Seeker
		*/
		function SeatSeekerSearchResult()
		{
		   
			  $tbl_name="seeker";
			  $curDate=date("Y-m-d");
			  $date=$_REQUEST['date'];
			  $sortby=$_REQUEST['sortby'];
			  if($sortby=='Date_Created')
			  {
			    $dsc="desc";
			  }
			  else
			  {
			    $dsc="asc";
			  }
			  		//your table name
	     // How many adjacent pages should be shown on each side?
	     $adjacents = 3;
	
		/* 
		   First get total number of rows in data table. 
		   If you have a WHERE clause in your query, make sure you mirror it here.
		*/
		$query = "SELECT COUNT(*) as num FROM $tbl_name";
		 if($_REQUEST['City']!='All')
			{
				$query.= " WHERE ".$this->m_strCityField." = '".$this->m_strCity."'";
			}
			if($_REQUEST['campus']!='All')
			{
				$query.= " WHERE ".$this->m_strCampusField." = '".$this->m_strCampus."'";
			}
			if($_REQUEST['Locality']!='')
			{
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!=''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']==''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!=''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']==''))
			{
				  $query.=" WHERE ";
			}
	        	 $query.=" ".$this->m_strLocalityField." = '".$this->m_strLocality."'";
			}
			if(($_REQUEST['SeatsFrom']!='')&&($_REQUEST['SeatsTo']!=''))
			{
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']==''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']==''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']==''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']==''))
			{
		 		 $query.=" WHERE ";
			}
				$query.= " ".$this->m_strSeatField." BETWEEN '".$this->m_nSeats."' AND '".$this->m_nSeatsTo."'";
			}
		 	if(($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!=''))
		 	{
				if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!=''))
				{
					 $query.=" AND ";
				}
				if(($_REQUEST['City']!='All')&&($_REQUEST['campus']==''))
				{
					 $query.=" AND ";
				}
				if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!=''))
				{
					 $query.=" AND ";
				}
				if(($_REQUEST['City']=='All')&&($_REQUEST['campus']==''))
				{
					  $query.=" WHERE ";
				}
			
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!='')&&($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')&&($_REQUEST['Locality']=='')))
			{
		 		 $query.=" WHERE ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!='')&&($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')&&($_REQUEST['Locality']!='')))
			{
		 		 $query.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['SeatsFrom']!='')&&($_REQUEST['SeatsTo']!='')))
			{
		 		 $query.=" AND ";
			}
		 		  $query.=" ".$this->m_strCostofSeatField." BETWEEN '".$this->m_nCostOfSeat."' AND '".$this->m_nCostOfSeatTo."'";
			} 
	 	    
			if($_REQUEST['date']!='')
	        {
	       if(($_REQUEST['date']!='D')&&($_REQUEST['City']!='All')&&($_REQUEST['campus']!='All'))
	       {
              $query.=" AND (";
            }
		  /* if(($_REQUEST['date']!='D')&&($_REQUEST['City']=='All')&&(($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')||($_REQUEST['CostFrom']=='')&&($_REQUEST['CostTo']=='')))
	       {
              $query.=" AND (";
            }*/
			if(($_REQUEST['date']!='D')&&($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')||($_REQUEST['CostFrom']=='')&&($_REQUEST['CostTo']=='')||($_REQUEST['Locality']=='')))
			{
			   $query.=" WHERE (";
			}
            if(($_REQUEST['date']!='D')&&($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['SeatsFrom']!='')&&($_REQUEST['SeatsTo']!='')||($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!='')||($_REQUEST['Locality']!='')))
			{
			   $query.=" AND (";
			}
           if($_REQUEST['date']=='A')
 	       {
		      $oneweek = mktime(0,0,0,date("m"),date("d")-7,date("Y"));
			  $oneweek_val = date("Y-m-d", $oneweek);
	          $query .= " provider.Date_Created BETWEEN '".$oneweek_val."' AND '".$curDate."' ";
	       }
	       if($_REQUEST['date']=='B')
	       {
		       $twoweek = mktime(0,0,0,date("m"),date("d")-14,date("Y"));
			   $twoweek_val = date("Y-m-d", $twoweek);
  	           $query .= " provider.Date_Created BETWEEN '".$twoweek_val."' AND '".$curDate."' ";
	        }
	       if($_REQUEST['date']=='C')
	       {
		      $onemonth = mktime(0,0,0,date("m"),date("d")-30,date("Y"));
			  $month_val = date("Y-m-d", $onemonth);
	          $query .= " provider.Date_Created BETWEEN '".$month_val."' AND '".$curDate."' ";
	       }
	     }
         if(($_REQUEST['date']=='A') || ($_REQUEST['date']=='B') || ($_REQUEST['date']=='C'))
	     {
             $query .=")";
	     }	
		$total_pages = mysql_fetch_array(mysql_query($query));
		$total_pages = $total_pages['num'];
		
		/* Setup vars for query. */
		$targetpage = "seekersearchresult.php"; 	//your file name  (the name of this file)
		$limit = 5; 								//how many items to show per page
		$page =@mysql_escape_string($_GET['page']);
		if($page) 
			$start = ($page - 1) * $limit; 			//first item to display on this page
		else
			$start = 0;						
 
		  $query1="SELECT * FROM $tbl_name";
		if($_REQUEST['City']!='All')
			{
				$query1.= " WHERE ".$this->m_strCityField." = '".$this->m_strCity."'";
			}
			if($_REQUEST['campus']!='All')
			{
				$query1.= " WHERE ".$this->m_strCampusField." = '".$this->m_strCampus."'";
			}
			if($_REQUEST['Locality']!='')
			{
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!=''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']==''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!=''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']==''))
			{
				  $query1.=" WHERE ";
			}
	        	 $query1.=" ".$this->m_strLocalityField." = '".$this->m_strLocality."'";
			}
			if(($_REQUEST['SeatsFrom']!='')&&($_REQUEST['SeatsTo']!=''))
			{
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']==''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']==''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']==''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']!='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']!=''))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='')&&($_REQUEST['Locality']==''))
			{
		 		 $query1.=" WHERE ";
			}
				$query1.= " ".$this->m_strSeatField." BETWEEN '".$this->m_nSeats."' AND '".$this->m_nSeatsTo."'";
			}
		 	if(($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!=''))
		 	{
				if(($_REQUEST['City']!='All')&&($_REQUEST['campus']!=''))
				{
					 $query1.=" AND ";
				}
				if(($_REQUEST['City']!='All')&&($_REQUEST['campus']==''))
				{
					 $query1.=" AND ";
				}
				if(($_REQUEST['City']=='All')&&($_REQUEST['campus']!=''))
				{
					 $query1.=" AND ";
				}
				if(($_REQUEST['City']=='All')&&($_REQUEST['campus']==''))
				{
					  $query1.=" WHERE ";
				}
			
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!='')&&($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')&&($_REQUEST['Locality']=='')))
			{
		 		 $query1.=" WHERE ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!='')&&($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')&&($_REQUEST['Locality']!='')))
			{
		 		 $query1.=" AND ";
			}
			if(($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['SeatsFrom']!='')&&($_REQUEST['SeatsTo']!='')))
			{
		 		 $query1.=" AND ";
			}
		 		  $query1.=" ".$this->m_strCostofSeatField." BETWEEN '".$this->m_nCostOfSeat."' AND '".$this->m_nCostOfSeatTo."'";
			} 
	 	    
			if($_REQUEST['date']!='')
	        {
	       if(($_REQUEST['date']!='D')&&($_REQUEST['City']!='All')&&($_REQUEST['campus']!='All'))
	       {
              $query1.=" AND (";
            }
		  /* if(($_REQUEST['date']!='D')&&($_REQUEST['City']=='All')&&(($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')||($_REQUEST['CostFrom']=='')&&($_REQUEST['CostTo']=='')))
	       {
              $query.=" AND (";
            }*/
			if(($_REQUEST['date']!='D')&&($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['SeatsFrom']=='')&&($_REQUEST['SeatsTo']=='')||($_REQUEST['CostFrom']=='')&&($_REQUEST['CostTo']=='')||($_REQUEST['Locality']=='')))
			{
			   $query1.=" WHERE (";
			}
            if(($_REQUEST['date']!='D')&&($_REQUEST['City']=='All')&&($_REQUEST['campus']=='All')&&(($_REQUEST['SeatsFrom']!='')&&($_REQUEST['SeatsTo']!='')||($_REQUEST['CostFrom']!='')&&($_REQUEST['CostTo']!='')||($_REQUEST['Locality']!='')))
			{
			   $query1.=" AND (";
			}
           if($_REQUEST['date']=='A')
 	       {
		      $oneweek = mktime(0,0,0,date("m"),date("d")-7,date("Y"));
			  $oneweek_val = date("Y-m-d", $oneweek);
	          $query1 .= " seeker.Date_Created BETWEEN '".$oneweek_val."' AND '".$curDate."' ";
	       }
	       if($_REQUEST['date']=='B')
	       {
		       $twoweek = mktime(0,0,0,date("m"),date("d")-14,date("Y"));
			   $twoweek_val = date("Y-m-d", $twoweek);
  	           $query1.= " seeker.Date_Created BETWEEN '".$twoweek_val."' AND '".$curDate."' ";
	        }
	       if($_REQUEST['date']=='C')
	       {
		      $onemonth = mktime(0,0,0,date("m"),date("d")-30,date("Y"));
			  $month_val = date("Y-m-d", $onemonth);
	          $query1 .= " seeker.Date_Created BETWEEN '".$month_val."' AND '".$curDate."' ";
	       }
	     }
         if(($_REQUEST['date']=='A') || ($_REQUEST['date']=='B') || ($_REQUEST['date']=='C'))
	     {
             $query1 .=")";
	     }	
	     
			$query1.=" order by $sortby $dsc LIMIT $start, $limit";
		  $this->setResultValue($query1);
		  //$result=mysql_query($query1);
		  
		  /* Setup page vars for display. */
		if ($page == 0) $page = 1;					//if no page var is given, default to 1.
		$prev = $page - 1;							//previous page is page - 1
		$next = $page + 1;							//next page is page + 1
		$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
		$lpm1 = $lastpage - 1;						//last page minus 1
		
		/* 
			Now we apply our rules and draw the pagination object. 
			We're actually saving the code to a variable in case we want to draw it more than once.
		*/
		$pagination = "";
		if($lastpage > 1)
		{	
			$pagination .= "<ul class=\"pagination pagination-sm\">";
			//previous button
			if ($page > 1) 
				$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$prev\">previous</a>";
			else
				$pagination.= "<li class=\"disabled\"><span>previous</span></li>";	
			
			//pages	
			if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
			{	
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li class=\"active\"><span>$counter</span></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$counter\">$counter</a></li>";					
				}
			}
			elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if($page < 1 + ($adjacents * 2))		
				{
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active\"><span>$counter</span></li>";
						else
							$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$counter\">$counter</a></li>";					
					}
					$pagination.= "<li><span>...</span></li>";
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$lpm1\">$lpm1</a></li>";
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$lastpage\">$lastpage</a></li>";		
				}
				//in middle; hide some front and some back
				elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
				{
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=1\">1</a></li>";
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=2\">2</a></li>";
					$pagination.= "...";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active\"><span>$counter</span></li>";
						else
							$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$counter\">$counter</a></li>";					
					}
					$pagination.= "<li><span>...</span></li>";
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$lpm1\">$lpm1</a></li>";
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$lastpage\">$lastpage</a></li>";		
				}
				//close to end; only hide early pages
				else
				{
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=1\">1</a></li>";
					$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=2\">2</a></li>";
					$pagination.= "<li><span>...</span></li>";
					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active\"><span>$counter</span></li>";
						else
							$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$counter\">$counter</a></li>";					
					}
				}
			}
			
			//next button
			if ($page < $counter - 1) 
				$pagination.= "<li><a href=\"$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Campus=".$this->getCampus()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&date=$date&sortby=$sortby&page=$next\">next</a></li>";
			else
				$pagination.= "<li class=\"disabled\"><span>next</span></li>";
			$pagination.= "</ul>\n";	
			$this->setPagination($pagination);	
		}
		}//function ends
		/*
		 Name:ViewProviderDetails()
		 Input: None
		 Output: Display the provider full details 
		*/
		function ViewProviderDetails()
		{
		   $query= "select * from provider where ProviderID=".$this->m_nID."";
		   $this->setResultValue($query);
		}//function ends
	    /*
		 Name:ViewProviderDetails()
		 Input: None
		 Output: Display the provider full details 
		*/
		function ViewSeekerDetails()
		{
		   $query= "select * from seeker where SeekerID=".$this->m_nID."";
		   $this->setResultValue($query);
		}//function ends
	  }//class ends
?>