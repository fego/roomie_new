<?php
	class cOffice
	{
	   private $m_nID;
	   private $m_nSeats;
	   private $m_nSeatsTo;
	   private $m_nCostOfSeat;
	   private $m_nCostofSeatTo;
	   private $m_strName;
	   private $m_strEmail;
	   private $m_nMobile;
	   private $m_strCity;
	   private $m_strLocality;
	   private $m_strAddress;
	   private $m_strDescription;
	   private $m_nRent;
	   private $m_nRentTo;
	   private $m_nAdvance;
	   private $m_strAC;
	   private $m_strBroadband;
	   private $m_strWater;
	   private $m_strPrinter;
	   private $m_strLaptop;
	   private $m_strParking;
	   private $m_strReception;
	   private $m_strKitchen;
	   private $m_strCafeteria;
	   private $m_strPublicTransport;
	   private $m_strNearestStop;
	   private $m_nDistance;
	   private $m_nDate;
	   private $m_strSeatField;
	   private $m_strCostofSeatField;
	   private $m_strRentField;
	   private $m_strCityField;
	   private $m_strLocalityField;
	   private $m_strDBResult;
	   private $m_nPage;
	   function setSeatField($seatfield)
	   {
	      $this->m_strSeatField=$seatfield;
	   }
	   function setCostofSeatField($costfield)
	   {
	      $this->m_strCostofSeatField=$costfield;
	   }
	   function setRentField($rentfield)
	   {
	     $this->m_strRentField=$rentfield;
	   }
	   function setCityField($cityfield)
	   {
	     $this->m_strCityField=$cityfield;
	   }
	   function setLocalityField($locality)
	   {
	      $this->m_strLocalityField=$locality;
	   }
	   function setResultValue($result)
	   {
	      $this->m_strDBResult=$result;
	   }
	   function getResultValue()
	   {
	     return $this->m_strDBResult;
	   }
	   function setID($ID)
	   {
	     $this->m_nID=$ID;
	   }
	   function setNumberofSeats($seats)
	   {
	      $this->m_nSeats=$seats;
	   }
	   function getNumberofSeats()
	   {
	      return $this->m_nSeats;
	   }
	   function setNumberofSeatsTo($seats)
	   {
	      $this->m_nSeatsTo=$seats;
	   }
	   function getNumberofSeatsTo()
	   {
	      return $this->m_nSeatsTo;
	   }
	   function setCostofSeat($cost)
	   {
	      $this->m_nCostOfSeat= $cost;
	   }
	  
	   function getCostofSeat()
	   {
	     return $this->m_nCostofSeat;
	   }
	    function setCostofSeatTo($costto)
	   {
	      $this->m_nCostOfSeatTo= $costto;
	   }
	   function getCostofSeatTo()
	   {
	      return $this->m_nCostOfSeatTo;
	   }
	   function setName($name)
	   {
	     $this->m_strName=$name;
	   }
	   function setEmail($email)
	   {
	     $this->m_strEmail=$email;
	   }
	   function setMobileNumber($mobile)
	   {
	     $this->m_nMobile=$mobile;
	   }
	   function setCity($city)
	   {
	      $this->m_strCity=$city;
	   }
	   function getCity()
	   {
	      return $this->m_strCity;
	   }
	   function setLocality($locality)
	   {
	      $this->m_strLocality=$locality;
	   }
	   function getLocality()
	   {
	      return $this->m_strLocality;
	   }
	   function setAddress($address)
	   {
	     $this->m_strAddress=$address;
	   }
	   function setDescription($description)
	   {
	     $this->m_strDescription=$description;
	   }
	   function setRent($rent)
	   {
	     $this->m_nRent=$rent;
	   }
	   function getRent()
	   {
	     return $this->m_nRent;
	   }
	    function setRentTo($rent)
	   {
	     $this->m_nRentTo=$rent;
	   }
	   function getRentTo()
	   {
	     return $this->m_nRentTo;
	   }
	   function setAdvance($advance)
	   {
	     $this->m_nAdvance=$advance;
	   }
	   function setAC($AC)
	   {
	     $this->m_strAC=$AC;
	   }
	   function setBroadBand($broadband)
	   {
	     $this->m_strBroadband=$broadband;
	   }
	   function setWater($water)
	   {
	     $this->m_strWater=$water;
	   }
	   function setPrinter($printer)
	   {
	     $this->m_strPrinter=$printer;
	   }
	   function setLaptop($pc)
	   {
	     $this->m_strLaptop=$pc;
	   }
	   function setParking($parking)
	   {
	     $this->m_strParking=$parking;
	   }
	   function setReception($reception)
	   {
	      $this->m_strReception=$reception;
	   }
	   function setKitchen($kitchen)
	   {
	      $this->m_strKitchen=$kitchen;
	   }
       function setCafeteria($cafeteria)
	   {
	      $this->m_strCafeteria=$cafeteria;
	   }
	   function setPublicTransport($transport)
	   {
	     $this->m_strPublicTransport=$transport;
	   }
	   function setNearestStop($nearest)
	   {
	     $this->m_strNearestStop=$nearest;
	   }
	   function setDistance($distance)
	   {
	     $this->m_strDistance=$distance;
	   }
	   function setDate($date)
	   {
	      $this->m_nDate=$date;
	   }
	   function setPagination($page)
	   {
	     $this->m_nPage=$page;
	   }
	   function getPagination()
	   {
	     return $this->m_nPage;
	   }
	   /*
		 Name:PostSeatProvider()
		 Input: None
		 Output: store the user data to provider table 
		*/
	   function PostSeatProvider()
	   {
	     echo $query="INSERT INTO provider(ProviderID,Seats,CostofSeat,Name,Email,Mobile,City,Locality,Address,Description,Rent,Advance,AC,Broadband,Water,Printer,Laptop,Parking,Reception,Kitchen,Cafeteria,PublicTransport,NearestStop,Distance,Date_Created) VALUES('".$this->m_nID."','".$this->m_nSeats."','".$this->m_nCostOfSeat."','".$this->m_strName."','".$this->m_strEmail."','".$this->m_nMobile."','".$this->m_strCity."','".$this->m_strLocality."','".$this->m_strAddress."','".$this->m_strDescription."','".$this->m_nRent."','".$this->m_nAdvance."','".$this->m_strAC."','".$this->m_strBroadband."','".$this->m_strWater."','".$this->m_strPrinter."','".$this->m_strLaptop."','".$this->m_strParking."','".$this->m_strReception."','".$this->m_strKitchen."','".$this->m_strCafeteria."','".$this->m_strPublicTransport."','".$this->m_strNearestStop."','".$this->m_strDistance."','".$this->m_nDate."')";
		 $res=mysql_query($query);
	   }//function ends
	    /*
		 Name:PostSeatSeeker()
		 Input: None
		 Output: store the user data to seeker table 
		*/
	    function PostSeatSeeker()
	   {
	     echo $query="INSERT INTO seeker(SeekerID,SeatsFrom,SeatsTo,CostofSeatFrom,CostofSeatTo,Name,Email,Mobile,City,Locality,Address,Description,RentFrom,RentTo,Advance,AC,Broadband,Water,Printer,Laptop,Parking,Reception,Kitchen,Cafeteria,PublicTransport,NearestStop,Distance,Date_Created) VALUES('".$this->m_nID."','".$this->m_nSeats."','".$this->m_nSeatsTo."','".$this->m_nCostofSeat."','".$this->m_nCostofSeatTo."','".$this->m_strName."','".$this->m_strEmail."','".$this->m_nMobile."','".$this->m_strCity."','".$this->m_strLocality."','".$this->m_strAddress."','".$this->m_strDescription."','".$this->m_nRent."','".$this->m_nRentTo."','".$this->m_nAdvance."','".$this->m_strAC."','".$this->m_strBroadband."','".$this->m_strWater."','".$this->m_strPrinter."','".$this->m_strLaptop."','".$this->m_strParking."','".$this->m_strReception."','".$this->m_strKitchen."','".$this->m_strCafeteria."','".$this->m_strPublicTransport."','".$this->m_strNearestStop."','".$this->m_nDistance."','".$this->m_nDate."')";
		 $res=mysql_query($query);
	   }//function ends
	    /*
		 Name:SeatProviderSearchResult()
		 Input: None
		 Output: retrive the user match criteria and give the result of seats provider
		*/
		function SeatProviderSearchResult()
		{
		   
			 $dis =$_REQUEST['display'];
                        
			 $tableName="provider";		
             
	         $targetpage = "providersearchresult_t1.php"; 	
	         $limit =$dis; 
        //$rent=$_REQUEST['sortby'];
	         $query = "SELECT COUNT(*) as num FROM $tableName WHERE  ".$this->m_strSeatField." BETWEEN '".$this->m_nSeats."' AND '".$this->m_nSeatsTo."' AND ".$this->m_strCostofSeatField." BETWEEN '".$this->m_nCostOfSeat."' AND '".$this->m_nCostOfSeatTo."' AND ".$this->m_strCityField." = '".$this->m_strCity."'";
	       if($_REQUEST['Locality']!='')
	       {
	         $query.=" AND ".$this->m_strLocalityField." = '".$this->m_strLocality."'";
	       }
	      if(($_REQUEST['RentFrom']!='')&&($_REQUEST['RentTo']!=''))
	      {
	        $query.=" AND ".$this->m_strRentField." BETWEEN ".$this->m_nRent." AND ".$this->m_nRentTo."";
	      }	
		
	       //echo $query;
	      $total_pages = @mysql_fetch_array(mysql_query($query));
          //echo $total_pages;
			$total_pages = $total_pages['num'];
		
		   //echo $total_pages;
		
		
			$stages = 3;
		
		
			$page = @mysql_escape_string($_GET['page']);
		
			if($page){
		
			$start = ($page - 1) * $limit; 
	
		    }else{
	
			$start = 0;	
	
	
		     }		
           // Get page data
	         $query1 = "SELECT * FROM $tableName WHERE  ".$this->m_strSeatField." BETWEEN '".$this->m_nSeats."' AND '".$this->m_nSeatsTo."' AND ".$this->m_strCostofSeatField." BETWEEN '".$this->m_nCostOfSeat."' AND '".$this->m_nCostOfSeatTo."' AND ".$this->m_strCityField." = '".$this->m_strCity."'";
	       if($_REQUEST['Locality']!='')
	       {
	         $query1.=" AND ".$this->m_strLocalityField." = '".$this->m_strLocality."'";
	       }
	      if(($_REQUEST['RentFrom']!='')&&($_REQUEST['RentTo']!=''))
	      {
	        $query1.=" AND ".$this->m_strRentField." BETWEEN ".$this->m_nRent." AND ".$this->m_nRentTo."";
	      }	
				
			$query1.=" order by Seats asc LIMIT $start, $limit";
            
			
            //echo $query1;
            $this->setResultValue($query1);
	       
             
			// Initial page num setup
		
			if ($page == 0){$page = 1;}
		
			$prev = $page - 1;	
		
			$next = $page + 1;							
		
			$lastpage = ceil($total_pages/$limit);		
		    echo $lastpage;
			$LastPagem1 = $lastpage - 1;						
		
			$paginate = '';
             echo "hi";
			if($lastpage > 1)
		
			{		
                
			$paginate .= "<div class='paginate'>";
	
			// Previous
	
			if ($page > 1){
			$paginate.= "<a href='$targetpage?SeatsFrom=".$this->getNumberofSeats()."&SeatsTo=".$this->getNumberofSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&display=$dis&page=$prev'><b>Prev</b></a>";
			}else{
			$paginate.= "<span class='disabled'>Prev</span>";	}		
			// Pages	
	
			if ($lastpage < 7 + ($stages * 2))	// Not enough pages to breaking it up
	
			{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page){
					$paginate.= "<span class='current'>$counter</span>";
				}else{
			 $paginate.= "<a href='$targetpage?SeatsFrom=".$this->getSeats()."&SeatsTo=".$this->getSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&display=$dis&page=$counter'>$counter</a>";}					
			   }
		    }
			elseif($lastpage > 5 + ($stages * 2))	// Enough pages to hide a few?
	
	
			{
			// Beginning only hide later pages
			if($page < 1 + ($stages * 2))		
			{
				for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
				{
					if ($counter == $page){
						$paginate.= "<span class='current'>$counter</span>";
					}else{
						$paginate.= "<a href='$targetpage?SeatsFrom=".$this->getSeats()."&SeatsTo=".$this->getSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&display=$dis&page=$counter'>$counter</a>";}					
				}
				$paginate.= "...";
				$paginate.= "<a href='$targetpage?SeatsFrom=".$this->getSeats()."&SeatsTo=".$this->getSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&display=$dis&page=$LastPagem1'>$LastPagem1</a>";
				$paginate.= "<a href='$targetpage?SeatsFrom=".$this->getSeats()."&SeatsTo=".$this->getSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&display=$dis&page=$lastpage'>$lastpage</a>";		
			}
			// Middle hide some front and some back
			elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
			{
				$paginate.= "<a href='$targetpage?SeatsFrom=".$this->getSeats()."&SeatsTo=".$this->getSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&display=$dis&page=1'>1</a>";
				$paginate.= "<a href='$targetpage?SeatsFrom=".$this->getSeats()."&SeatsTo=".$this->getSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&display=$dis&page=2'>2</a>";
				$paginate.= "...";
				for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
				{
					if ($counter == $page){
						$paginate.= "<span class='current'>$counter</span>";
					}else{
						$paginate.= "<a href='$targetpage?SeatsFrom=".$this->getSeats()."&SeatsTo=".$this->getSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&display=$dis&page=$counter'>$counter</a>";}					
				}
				$paginate.= "...";
				$paginate.= "<a href='$targetpage?page=$LastPagem1'>$LastPagem1</a>";
				$paginate.= "<a href='$targetpage?page=$lastpage'>$lastpage</a>";		
			}
			// End only hide early pages
			else
			{
				$paginate.= "<a href='$targetpage?SeatsFrom=".$this->getSeats()."&SeatsTo=".$this->getSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&display=$dis&page=1'>1</a>";
				$paginate.= "<a href='$targetpage?page=2'>2</a>";
				$paginate.= "...";
				for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page){
						$paginate.= "<span class='current'>$counter</span>";
					}else{
						$paginate.= "<a href='$targetpage?SeatsFrom=".$this->getSeats()."&SeatsTo=".$this->getSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&display=$dis&page=$counter'>$counter</a>";}					
				}
			}
		}
		// Next
		     if ($page < $counter - 1){ 
			$paginate.= "<a href='$targetpage?SeatsFrom=".$this->getSeats()."&SeatsTo=".$this->getSeatsTo()."&CostFrom=".$this->getCostofSeat()."&CostTo=".$this->getCostofSeatTo()."&City=".$this->getCity()."&Locality=".$this->getLocality()."&RentFrom=".$this->getRent()."&RentTo=".$this->getRentTo()."&display=$dis&page=$next'><b>Next</b></a>";
		    }else{
			$paginate.= "<span class='disabled'>Next</span>";
			}			
		   $paginate.= "</div>";
		   echo "page";
		   $this->setPagination($paginate);		
         }	
		
		
		}//function ends
	   
	  }//class ends
?>