<?php
include("db.php");
require_once("common_inc.php");
?>
<script type = "text/javascript">
function validate()
{
	var valid=document.form1;
  
        var erColor="#E8E8FF";
        var x=document.getElementById('provider');
	var y=document.getElementById('seeker');
if(x.checked==false&&y.checked==false)
 {
      alert("please select Viewby Provider / Seeker ID");
      document.getElementById("provider").focus();
      return false; 
 }
if(IsEmpty(valid.ID))
	{
		alert("Please Enter your  Provider / Seeker ID");
		valid.ID.style.background=erColor;
		valid.ID.focus();
		return false;
	}
	else
	{
	valid.ID.style.background= 'White';
	valid.ID.focus();
	}
function IsEmpty(obj)
{
	var objValue;
	objValue = obj.value.replace(/\s+$/,"");
	if(objValue.length == 0)
	{return true;} 
	else{return false;}
}
}
</script>
<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="pageTitle">View by Provider / Seeker ID</h2>
			</div>
		</div>
	</div>
</section>
<div class="container">
<p>&nbsp;</p><p>&nbsp;</p>
<form role="form" action="viewproviderdetails.php?active=VB" method="post"  name="form1" id="form1" onSubmit="return validate();">

	<div class="form-group">
    	<div class="row" align="right">
    	<div class="col-md-2"><span class="text-danger">*</span> <b>ID</b> :</div>
        <div class="col-md-2"><input name="ID" type="text" id="seekerid" class="form-control"></div>
        <div class="col-md-2"><input name="viewfor" type="radio" value="P" id="provider" /> Provider</div>
        <div class="col-md-2"><input name="viewfor" type="radio" value="S" id="seeker" /> Seeker</div>
        <div class="col-md-2"><button type="submit" name="Submit" class="btn btn-primary" value="Search">Search</button></div>
        </div>
    </div>
</form>
<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
</div><!--/.container-->