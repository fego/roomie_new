<?php
require_once("common_inc.php");
?>
<script type="text/javascript" src="js/search.js">
</script>
<script type="text/javascript">
function hide()
{
if(document.form1.postedby.value =='Roomie')
{
var tableRow = document.getElementById('ds');
tableRow.style.display = '';
//document.form1["age"].style.visibility = 'visible';
//document.form1["gender"].style.visibility = 'visible';
}
else
{
var tableRow = document.getElementById('ds');
tableRow.style.display = 'none';
//document.form1["age"].style.visibility = 'hidden';
//document.form1["gender"].style.visibility = 'hidden';
}
}
</script>
<body onLoad="hide();">
<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="pageTitle">Room Search</h2>
			</div>
		</div>
	</div>
</section>
<div class="container">
<p>&nbsp;</p>
<form role="form" id="form1" name="form1" method="post" action="roomsearchresult.php?active=RS" onSubmit="return roomvalidate()">
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-2"><span class="text-danger">*</span> <b>Posted By</b> :</div>
        <div class="col-md-4"><select name="postedby" id="postedby" onChange="hide();" class="form-control">
          <option selected="postedby" value="All">All</option>
          <option value="PG">PG</option>
          <option value="Hostel">Hostel</option>
          <option value="Owner">Owner</option>
          <option value="Agent">Agent</option>
          <option value="Roomie">Roomie</option>
        </select></div>
    	<div class="col-md-2"><b>Campus</b> :</div>
        <div class="col-md-4"><select name="campus" id="campus" class="form-control">
            <option selected="selected" value="">All</option>
            <?php
			require_once("common_inc.php");
			foreach ($arrCampus as $value)
				{
			echo '<option value="';echo $value;echo '";>';
			echo "$value";
			echo '</option>';echo "\n";
				}
		?>
        </select></div>
        </div>
    </div>
    <div class="form-group" id="ds">
    	<div class="row">
    	<div class="col-md-2"><span class="text-danger">*</span> <b>Age</b> :</div>
        <div class="col-md-1"><select name="minage" id="minage" class="form-control">
         <option selected="selected" value="">select</option>
           <?php
			 $nAge=17;
			 while($nAge++ < 40)
			 {
			 echo "<option value='";echo $nAge;echo "';>$nAge</option>";
			 }
			 ?>
          </select></div>
        <div class="col-md-2" align="center"> To </div>
        <div class="col-md-1"><select name="maxage" id="maxage" class="form-control">
          <option selected="selected" value="">select</option>
           <?php
			 $nAge=17;
			 while($nAge++ < 40)
			 {
			 echo "<option value='";echo $nAge;echo "';>$nAge</option>";
			 }
			 ?>
          </select></div>
        <div class="col-md-2"><span class="text-danger">*</span> <b>Gender</b> :</div>
        <div class="col-md-4"><select name="gender" id="gender" class="form-control">
            <option selected="gender" value="">select</option>
            <option value='M'>Male</option>
            <option value='F'>Female</option>
          </select></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-2"><span class="text-danger">*</span> <b>City</b> :</div>
        <div class="col-md-4"><select name="city" id="city" class="form-control">
		  <option selected="city" value="">select</option>
            <?php
			require_once("common_inc.php");
            foreach ($arrCity as $value)
  			 {
    			 echo '<option value="';echo $value;echo '";>';
    			 echo "$value";
   				  echo '</option>';echo "\n";
			 }
			?>
          </select></div>
        <div class="col-md-2"> <b>Locality</b> :</div>
        <div class="col-md-4"><select name="locality" id="locality" class="form-control">
          <option selected="locality" value="">All</option>
          <option value="Central">Central</option>
          <option value="North">North</option>
          <option value="South">South</option>
          <option value="East">East</option>
          <option value="West">West</option>
        </select></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-2"><span class="text-danger">*</span> <b>Rent</b> :</div>
        <div class="col-md-1"><input name="rentfrom" type="text" id="rentfrom" class="form-control"></div>
        <div class="col-md-2" align="center"> To </div>
        <div class="col-md-1"><input name="rentto" type="text" id="rentto" class="form-control"></div>
        <div class="col-md-2"> <b>Place</b> :</div>
        <div class="col-md-4"><input type="text" name="place" class="form-control"></div>
        </div>
    </div>
    <div class="form-group text-info"><b>Room Category</b></div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-3"><input type="checkbox" name="fullyfurnished" value="F"> Fully Furnished</div>
        <div class="col-md-3"><input type="checkbox" name="unfurnished" value="U"> Un Furnished</div>
        <div class="col-md-3"><input type="checkbox" name="semifurnished" value="S"> Semi Furnished</div>
        </div>
    </div>
    <div class="form-group text-info"><b>Room Type</b></div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-3"><input type="checkbox" name="singleroom" value="S"> Single Room</div>
        <div class="col-md-3"><input type="checkbox" name="onebhk" value="O"> 1 BHK</div>
        <div class="col-md-3"><input type="checkbox" name="twobhk" value="T"> 2 BHK</div>
        <div class="col-md-3"><input type="checkbox" name="threebhk" value="R"> 3 BHK</div>
        </div>
    </div>
    <div class="form-group text-info"><b>Posted in Last</b></div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-3"><input type="radio" name="date" value="A"> 1 Week</div>
        <div class="col-md-3"><input type="radio" name="date" value="B"> 2 Week</div>
        <div class="col-md-3"><input type="radio" name="date" value="C"> 1 Month</div>
        <div class="col-md-3"><input type="radio" name="date" value="D"> All</div>
        </div>
    </div>
    <div class="form-group text-info"><b>Others</b></div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-2"> <b>Vegetarian</b> :</div>
        <div class="col-md-4"><select name="veg" id="veg" class="form-control">
		  <option selected="selected" value="">select</option>
            <option value="Y">Yes</option>
            <option value="N">No</option>
          </select></div>
        <div class="col-md-2"> <b>Smoking</b> :</div>
         <div class="col-md-4"><select name="smoking" id="smoking" class="form-control">
		  <option selected="selected" value="">select</option>
            <option value="Y">Yes</option>
            <option value="N">No</option>
          </select></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
        <div class="col-md-2"> <b>Alcohol</b> :</div>
        <div class="col-md-4"><select name="alchoal" id="alchoal" class="form-control">
		  <option selected="selected" value="">select</option>
            <option value="Y">Yes</option>
            <option value="N">No</option>
          </select></div>
    	<div class="col-md-2"><span class="text-danger">*</span> <b>Sort By</b> :</div>
        <div class="col-md-2"><select name="sortby" id="sortby" class="form-control">
            <option selected="selected" value="Rent">Rent</option>
            <option value="Age">Age</option>
          </select></div>
        <div class="col-md-1" align="center"> Per Page </div>
        <div class="col-md-1"><select name="display" id="display" class="form-control">
            <option>10</option>
            <option>20</option>
            <option>30</option>
            <option>40</option>
            <option>50</option>
          </select></div>
        </div>
    </div>
	<div class="form-group" align="right">
        <button type="submit" name="Submit" class="btn btn-primary" value="Search">Search</button>
    </div>
	</form>
</div>