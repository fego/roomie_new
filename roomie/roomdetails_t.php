<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="pageTitle">Room Details</h2>
			</div>
		</div>
	</div>
</section>
<div class="container">
<p>&nbsp;</p>
<form role="form" name="form">
<?php
$rid = $_REQUEST['Room_Id'];
$sql = "SELECT * FROM rooms WHERE Room_Id = '$rid' AND IsActive='Y' ";
$result = mysql_query($sql);
if (!@mysql_num_rows($result))
{
  printf("<script>location.href='searchfailure.php?active=RS'</script>");
}
else
{
while(($row = @mysql_fetch_array($result))) 
{
?>
<div class="form-group">
<div class="row">
<div class="col-md-1">&nbsp;</div>
<div class="col-md-7">
	<div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Room ID</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Room_Id']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Date Posted</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Date_Created']; ?></div>
        </div>
    </div>
    <div class="form-group text-info"><b>Personal Information</b></div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Name</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Name']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Age</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Age']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Gender</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php 
          if($row['Gender']=='M')
          {
              echo "Male";
          }
          else
          {
              echo "Female";
           } 
           ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Campus</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Campus']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>City</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['City']; ?></div>
        </div>
    </div>
	<div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Locality</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Locality']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Place</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Place']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Rent</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Rent']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Advance</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Advance']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>About Me</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['About_Me']; ?></div>
        </div>
    </div>
    <div class="form-group text-info"><b>Contact Information</b></div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Mobile</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Phone']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Email</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Email']; ?></div>
        </div>
    </div>
    <div class="form-group text-info"><b>Additional Information</b></div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Native State</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['Native']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Room Details</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['AboutRoom']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Furnishing</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php 
	               $ufFlag = 0;
                     
			if($row["UnFurnished"]!='')
			{
                           $ufFlag = 1;
                        
                           if($ufFlag==1)
			   {
			    echo "Un Furnished";
                            }
			 }
				$sfFlag =0;
		         if($row["SemiFurnished"]!='')
		         {
                                $sfFlag =1;
			      if($ufFlag==1)
                              {
				 echo " OR SemiFurnished";
                              }
                              else
                              {
				  echo "SemiFurnished";
	                       }
			  }	
				$ffFlag=0;
                          if($row["FullyFurnished"]!='')
			  {
                             $ffFlag=1;
                             if($ufFlag==1 || $sfFlag==1)
			     {
				   echo " OR Fully Furnished";
			      }
                             else
			     {
                                   echo "Fully Furnished";
			      }
			   }
	   
	  ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Room Type</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php 
	         $srFlag = 0;
                        
		if($row["SingleRoom"]!='')
		{
                      $srFlag = 1;
                      
                       if($srFlag==1)
		       {
			   echo "SingleRoom";
                        }
		   }
		   $obFlag =0;
					
                    if($row["OneBhk"]!='')
		    {
                         $obFlag =1;
			  if($srFlag==1)
                          {
				echo " OR 1 BHK";
                           }
                           else
                           {
				echo "1 BHK";
                            }
 			}	
   
		       $tbFlag=0;
                         if($row["TwoBhk"]!='')
			 {
                             $tbFlag=1;
                             if($srFlag==1 || $obFlag==1)
			     {
				   echo " OR 2 BHK";
			     }
					
                             else
			     {
                                     echo "2 BHK";
			     }
			}
			$threebhkFlag=0;
						
                        if($row["ThreeBhk"]!='')
			{
                            $threebhkFlag=1;
                           if($srFlag==1 || $obFlag==1 || $tbFlag==1)
                            {
                                 echo " OR 3BHK";
                            }
					
                            else
                            {
				  echo "3 BHK";
                            }
		        }
	  ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Max.Roomies</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php echo $row['MaxRoomies']; ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Link</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php 
	  $link = $row['Link'];
	 echo "<a href=\"$link\">$link</a>";
	  
	   ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Language</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php 
	  if($row['Language']=='A')
	  {
	  echo "Tamil";
	  }
	  if($row['Language']=='B')
	  {
	  echo "Hindi";
	  }
	  if($row['Language']=='C')
	  {
	  echo "Telugu";
	  }
	  if($row['Language']=='D')
	  {
	  
	  echo "Kannada"; 
	  }
	  if($row['Language']=='E')
	  {
	  
	  echo "Malayalam"; 
	  }
	  if($row['Language']=='F')
	  {
	  
	  echo "English"; 
	  }
	  ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Vegetarian</b></div>

        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php
		  if($row['Vegetarian']=='N')
		  echo "No";
		  if($row['Vegetarian']=='Y')
		  echo "Yes"
	    ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Smoking</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php
		  if($row['Smoking']=='N')
		  echo "No";
		  if($row['Smoking']=='Y')
		  echo "Yes"
	    ?></div>
        </div>
    </div>
    <div class="form-group">
    	<div class="row">
    	<div class="col-md-4"><b>Alcohol</b></div>
        <div class="col-md-2">:</div>
        <div class="col-md-6"><?php
		  if($row['Alchoal']=='N')
		  echo "No";
		  if($row['Alchoal']=='Y')
		  echo "Yes"
	    ?></div>
        </div>
    </div>
</div>
<div class="col-md-4" align="center">
<?php 
        if($row['Photo']=="")
        {
        ?>
           <a href="#" data-toggle="modal" data-target=".pop-up-1"><img class="img-thumbnail" src="home.jpg" height="200" width="200" /></a>
        <?php
        }
        if($row['Photo']!=="")
        {
        ?>
        	<a href="#" data-toggle="modal" data-target=".pop-up-1"><img class="img-thumbnail" src="upload/<?php echo $row['Photo'];?>" height="200" width="200" /></a>
        <?php
        }
        ?>
</div></div></div>
 <!--  Modal content for the mixer image example -->
  <div class="modal fade pop-up-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        </div>
        <div class="modal-body">
		<?php
        if($row['Photo']=="")
        {
        ?>
        <img src="home.jpg" class="img-responsive img-rounded center-block" alt="">
        <?php
        }
        if($row['Photo']!="")
        {
        ?>
        <img src="upload/<?php echo $row['Photo']; ?>" class="img-responsive img-rounded center-block" alt="">
        <?php
        }
        ?>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal mixer image -->
  <?php } }?>
</form></div>